- [About Me](#about-me)
- [How we succeed](#how-we-succeed)
- [Communication](#communication)
- [Feedback](#feedback)
- [Working Hours](#working-hours)
- [One-to-one meetings (1:1’s)](#one-to-one-meetings-11s)

---

## About Me

My name is David Curlewis, although most people call me Dave.  While less relevant these days I was known for years as "DB Dave" (as in "[Database Dave](https://www.scoop.co.nz/stories/BU1203/S00738/trademe-database-dave-leads-microsoft-sql-server-user-group.htm)"). 

Find/Contact me here: 
* [LinkedIn](https://www.linkedin.com/in/dcurlewis/)
* [Email](mailto:david@curlewis.co.nz)

I grew up in South Africa and moved to New Zealand in February 2003. I'm based in [Wellington](https://en.wikipedia.org/wiki/Wellington), living with my partner, three daughters, two dogs, and a cat.

My last few roles: 
* [Trade Me](https://www.trademe.co.nz/) - Database Team lead (2007-2014)
* [Timely](https://www.gettimely.com/blog/database-dave-curlewis-joins-timely/) - Head of Platform Engineering (2014-2022)
* [Sharesies](https://www.sharesies.nz/) - Engineering Manager (2022-2022)
* [GitLab](https://www.gitlab.com/) - Engineering Manager, Reliability (2022-2023)
* [Flux Federation](https://www.fluxfederation.com/) - Engineering Manager (2023-2024)
* [Canva](https://www.canva.com/) - Senior Engineering Manager, ML Platform (2024-present)

My technical experience is broad. I started in the late 90's with sys-admin and networking, moved into a wide range of database-focused roles, before moving into people leadership since around 2010. The teams I've built and led have included Data, SRE, Platform Engineering, Infrastructure, Security, Cloud Engineering, and product delivery.

I'm an [Architect (INTJ-T)](https://www.16personalities.com/intj-personality) personality type, and being an introvert I find in-person networking and multi-day get-togethers taxing, but at the same time I thoroughly enjoy them and value the relationships resulting from these opportunities.

Outside of work, I enjoy hitting the gym (powerlifting), motorcycle riding, wood-working and PC gaming, among other things (I'm a bit of a "serial-hobbyist" to be honest).

## How we succeed

My job is, in a nutshell, to strive for better outcomes for the team. If my team is not happy & effective, then I'm not doing a good enough job. 

To encourage excellent outputs, I focus on ensuring the right people are working on the right tasks, minimizing friction before stepping back. I want to be an enabler without becoming an obstacle.

To ensure a healthy team culture I rely on attracting & retaining great people, transparent and honest communication, and fostering a sense of belonging and [psychological safety](https://www.thoughtworks.com/en-sg/insights/blog/product-innovation/psychological-safety-product-innovation) where team members are able to be themselves.

## Communication

Communication is key. I'm fairly direct in my style. We’re all adults and operate in a high-trust environment, so I default to trying to stay out of your way where possible, but this mode means that I may miss things unless you explicitly loop me in. I've always said that my only "rule" is that I don't want any surprises; what I mean by this these days is that I'd rather have too much information than not enough. So if there's anything I might need to know about, rather let me know and leave it to me to filter where necessary.

I encourage questions, but I don’t want to be a crutch.  So if you come to me with a problem, try to already have several suggestions in mind on how we might solve it which we can then discuss.

I'm a big proponent of [asynchronous ways of working](https://remote.com/blog/why-you-should-be-doing-async-work), and I believe this can make everyone more efficient regardless of whether you're [globally remote](https://about.gitlab.com/company/culture/all-remote/getting-started/) or everyone's in the same office.

## Feedback

Constructive feedback is critical to improvement; whether we're talking about technical systems, or us as people. Delivering and receiving feedback can be challenging. I try to leverage [Radical Candor](https://review.firstround.com/radical-candor-the-surprising-secret-to-being-a-good-boss), and to deliver feedback with respect, [assuming positive intent](https://collaborativeway.com/general/a-ceos-advice-assume-positive-intent/) on all sides. 

I appreciate timely feedback, ideally delivered soon after the relevant event. Delivery should ideally be via a direct message, or a face-to-face call/chat. I may ask for some time to compile my thoughts before I respond. 

If we work together then I would appreciate being told how you prefer to receive feedback, and I will likely ask you this early on during our 1-on-1's.

## Working Hours

My timezone is [New Zealand Standard Time](https://www.timeanddate.com/time/zone/new-zealand) (UTC +12), or New Zealand Daylight Time in summer (UTC +13). My normal working hours are Mon-Fri, _roughly_ 09:00-19:00, depending on the day.

I respect your personal time. I may at times message or email you outside of normal hours, but I absolutely _don’t_ expect you to reply or action anything until your normal working hours.

The exception to this, of course, involves operational issues, agreed-upon after-hours support, on-call rosters, and similar scenarios. 

I am available 24x7 if something is urgent. If in doubt, err on the side of caution and loop me in. A phone call or txt are best if you need to get hold of me ASAP.

## One-on-one meetings (1:1s)

I value 1:1s above most other meetings, so will defend our time as much as possible. If something unavoidable comes up I will work with you to reschedule our catch-up, not cancel it.

This is largely _your_ time and _your_ agenda. We don’t need to talk about your projects, there are other mechanisms for that. This time is for you to give and receive feedback, ask questions, rant if necessary, or simply chat about your weekend; whatever you need.

We will maintain a private shared running document (Confluence, Google Doc, etc - depending on our work environment) documenting our agenda and meeting notes for each meeting. Feel free to update this whenever you want to.

I like to have a more in-depth 1:1 roughly once a quarter, focussing more on your personal development and feedback.

## Iteration

This is a living document which I _try_ to keep updated. If, however, there is anything you were hoping to learn about me but isn't contained above, please get in touch (Slack DM if we work together, or email if not) to let me know. 